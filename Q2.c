#include <stdio.h>
int fibonacciSeq (int);
int main()
{
	int number,i=0,Row;
	printf("Enter the number : \n");
	scanf("%d", &number);
	
	for(i=0;i<number;i++)
	{
	Row = fibonacciSeq(i);
	printf("%d \n", Row );
	}
	return 0;
}
int fibonacciSeq(int num)
{
	int result;
	if (num==0  || num == 1)
	{ 
	return num;
	}
	else
	{
	result = fibonacciSeq(num-1) + fibonacciSeq(num-2);
	return  result ;
	}
}

